﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Turkiye;

namespace Turkiye
{
    public partial class frm_adana : Form
    {
        public frm_adana()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 index = new Form1();
            index.Show();
            this.Hide();
        }

        private void frm_adana_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            turkiyeEntities db = new turkiyeEntities();
            var linqList = (from adn in db.adana.ToList()
                            where adn.ilkodu == 1
                            select new { adn.genelbilgi }).ToList();

            dgv.DataSource = linqList;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            turkiyeEntities db = new turkiyeEntities();
            var linqList = (from adn in db.adana.ToList()
                            where adn.ilkodu == 1
                            select new { adn.tarihiyerler }).ToList();

            dgv.DataSource = linqList;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            turkiyeEntities db = new turkiyeEntities();
            var linqList = (from adn in db.adana.ToList()
                            where adn.ilkodu == 1
                            select new { adn.yapmadandonme }).ToList();

            dgv.DataSource = linqList;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            turkiyeEntities db = new turkiyeEntities();
            var linqList = (from adn in db.adana.ToList()
                            where adn.ilkodu == 1
                            select new { adn.ulasim }).ToList();

            dgv.DataSource = linqList;
        }

       
    }
}
