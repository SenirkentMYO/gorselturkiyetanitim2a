﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Turkiye
{
    public partial class frm_afyon : Form
    {
        public frm_afyon()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 index = new Form1();
            index.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            turkiyeEntities db = new turkiyeEntities();
            var linqList = (from afyn in db.afyon.ToList()
                            where afyn.ilkodu == 3
                            select new { afyn.genelbilgi }).ToList();

            dgv.DataSource = linqList;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            turkiyeEntities db = new turkiyeEntities();
            var linqList = (from afyn in db.afyon.ToList()
                            where afyn.ilkodu == 3
                            select new { afyn.tarihiyerler }).ToList();

            dgv.DataSource = linqList;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            turkiyeEntities db = new turkiyeEntities();
            var linqList = (from afyn in db.afyon.ToList()
                            where afyn.ilkodu == 3
                            select new { afyn.yapmadandonme }).ToList();

            dgv.DataSource = linqList;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            turkiyeEntities db = new turkiyeEntities();
            var linqList = (from afyn in db.afyon.ToList()
                            where afyn.ilkodu == 3
                            select new { afyn.ulasim }).ToList();

            dgv.DataSource = linqList;
        }
    }
}
