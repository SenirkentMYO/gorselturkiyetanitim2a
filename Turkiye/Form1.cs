﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Turkiye;

namespace Turkiye
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            frm_adana adana = new frm_adana();
            adana.Show();
            this.Hide();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            frm_adiyaman adiyaman = new frm_adiyaman();
            adiyaman.Show();
            this.Hide();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            frm_afyon afyon = new frm_afyon();
            afyon.Show();
            this.Hide();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            frm_agri agri = new frm_agri();
           agri.Show();
            this.Hide();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            frm_amasya amasya = new frm_amasya();
            amasya.Show();
            this.Hide();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            frm_ankara ankara = new frm_ankara();
            ankara.Show();
            this.Hide();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            frm_antalya antalya = new frm_antalya();
            antalya.Show();
            this.Hide();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            frm_artvin artvin = new frm_artvin();
            artvin.Show();
            this.Hide();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            frm_aydin aydin = new frm_aydin();
            aydin.Show();
            this.Hide();
        }

        private void label10_Click(object sender, EventArgs e)
        {
            frm_balikesir balikesir = new frm_balikesir();
            balikesir.Show();
            this.Hide();

        }

        private void label11_Click(object sender, EventArgs e)
        {
            frm_bilecik bilecik = new frm_bilecik();
            bilecik.Show();
            this.Hide();
        }

        private void label12_Click(object sender, EventArgs e)
        {
            frm_bingol bingol = new frm_bingol();
            bingol.Show();
            this.Hide();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            frm_bitlis bitlis = new frm_bitlis();
            bitlis.Show();
            this.Hide();
        }

        private void label14_Click(object sender, EventArgs e)
        {
            frm_bolu bolu = new frm_bolu();
            bolu.Show();
            this.Hide();
        }
       

        private void label15_Click(object sender, EventArgs e)
        {
            frm_burdur burdur = new frm_burdur();
            burdur.Show();
            this.Hide();
        }

        private void label16_Click(object sender, EventArgs e)
        {
            frm_bursa bursa = new frm_bursa();
            bursa.Show();
            this.Hide();
        }

        private void label17_Click(object sender, EventArgs e)
        {
            frm_canakkale canakkale = new frm_canakkale();
            canakkale.Show();
            this.Hide();
        }

        private void label18_Click(object sender, EventArgs e)
        {
            frm_cankiri cankiri = new frm_cankiri();
            cankiri.Show();
            this.Hide();
        }

        private void label19_Click(object sender, EventArgs e)
        {
            frm_corum corum = new frm_corum();
            corum.Show();
            this.Hide();
        }

        private void label20_Click(object sender, EventArgs e)
        {
            frm_denizli denizli = new frm_denizli();
            denizli.Show();
            this.Hide();
        }

        private void label21_Click(object sender, EventArgs e)
        {
            frm_diyarbakir diyarbakir = new frm_diyarbakir();
            diyarbakir.Show();
            this.Hide();
        }

        private void label22_Click(object sender, EventArgs e)
        {
            frm_edirne edirne= new frm_edirne();
            edirne.Show();
            this.Hide();
        }

        private void label23_Click(object sender, EventArgs e)
        {
            frm_elazig elazig = new frm_elazig();
            elazig.Show();
            this.Hide();
        }

        private void label24_Click(object sender, EventArgs e)
        {
            frm_erzincan erzincan = new frm_erzincan();
            erzincan.Show();
            this.Hide();
        }

        private void label25_Click(object sender, EventArgs e)
        {
            frm_erzurum erzurum = new frm_erzurum();
            erzurum.Show();
            this.Hide();
        }

        private void label26_Click(object sender, EventArgs e)
        {
            frm_eskisehir eskisehir = new frm_eskisehir();
            eskisehir.Show();
            this.Hide();
        }

        private void label27_Click(object sender, EventArgs e)
        {
            frm_gaziantep gaziantep = new frm_gaziantep();
            gaziantep.Show();
            this.Hide();
        }

        private void label28_Click(object sender, EventArgs e)
        {
            frm_giresun giresun = new frm_giresun();
            giresun.Show();
            this.Hide();
        }

        private void label29_Click(object sender, EventArgs e)
        {
            frm_gumushane gumushane = new frm_gumushane();
            gumushane.Show();
            this.Hide();
        }

        private void label30_Click(object sender, EventArgs e)
        {
            frm_hakkari hakkari = new frm_hakkari();
            hakkari.Show();
            this.Hide();
        }
    }
}
