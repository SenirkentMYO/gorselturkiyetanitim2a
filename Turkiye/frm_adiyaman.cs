﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Turkiye
{
    public partial class frm_adiyaman : Form
    {
        public frm_adiyaman()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 index = new Form1();
            index.Show();
            this.Hide();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            turkiyeEntities db = new turkiyeEntities();
            var linqList = (from adymn in db.adiyaman.ToList()
                            where adymn.ilkodu == 2
                            select new { adymn.genelbilgi }).ToList();

            dgv.DataSource = linqList;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            turkiyeEntities db = new turkiyeEntities();
            var linqList = (from adymn in db.adiyaman.ToList()
                            where adymn.ilkodu == 2
                            select new { adymn.tarihiyerler }).ToList();

            dgv.DataSource = linqList;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            turkiyeEntities db = new turkiyeEntities();
            var linqList = (from adymn in db.adiyaman.ToList()
                            where adymn.ilkodu == 2
                            select new { adymn.yapmadandonme }).ToList();

            dgv.DataSource = linqList;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            turkiyeEntities db = new turkiyeEntities();
            var linqList = (from adymn in db.adiyaman.ToList()
                            where adymn.ilkodu == 2
                            select new { adymn.ulasim }).ToList();

            dgv.DataSource = linqList;
        }
    }
}
